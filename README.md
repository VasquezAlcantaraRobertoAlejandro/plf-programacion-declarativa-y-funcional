# Mapa conceptual 1
## Programación Declarativa. Orientaciones y pautas para el estudio (1998, 1999, 2001)

```plantuml
@startmindmap
*[#DAA520] Programación
	*_ Se conforma de dos partes
		*[#F08080] Creativa
			*_ Son
				*[#00BFFF] Ideas
				*[#00BFFF] Algoritmos
				*[#00BFFF] Clara con la forma de resolver un problema
			*[#00BFFF] No hay que tener una idea en especial para\nresolver una tarea en especial
		*[#F08080] Burocrática
			*_ Son
				*[#00BFFF] Gestión detallada de la memoria del ordenador 
					*_ que
						*[#9ACD32] siguen una secuencia de ordenes
							*_ las cuales
								*[#8FBC8F] nos lleva a la solución deseada.
			*_ Ejemplo
				*[#00BFFF] Como ejemplo está el código máquina 
					*_ en el cual
						*[#9ACD32] se debe llevar acabo la gestión de\nmemoria para resolver un problema
	*_ Hay dos tipos
		*[#F08080] Programación Iperativa
			*_ Caracterizada por
				*[#00BFFF] Dar muchos detalles sobre los calculos \n que se desan realizar.
				*[#00BFFF] Se modifica el estado de memoria del ordenador.
		*[#F08080] Programación Declarativa
			*_ Caracterizada por
				*[#00BFFF] Liberarse de las asignaciones
				*[#00BFFF] Liberarse de detallar especificaciones
				*[#00BFFF] Hacer uso de los recursos expresivos
					*_ los cuales 
						*[#9ACD32] permiten especificar los programas a un 
							*[#8FBC8F] Nivel más alto
							*[#8FBC8F] Más cercano al pensamiento del programador
			*_ Se abandonan
				*[#00BFFF] las secuencias de ordenes que gestionan la memoria del ordenador
					*_ esto
						*[#9ACD32] para tomar la perspectiva de darle explicaciones.
			*_ Se conforma de dos tipos de lenguajes
				*[#00BFFF] Funcional
					*_ Donde se toma a \nla programación como
						*[#9ACD32] La aplicación de funciones a datos realizados 
							*_ mediante 
								*[#8FBC8F] reglas de rescritura 
								*[#8FBC8F] simplificación de expresiones
						*[#9ACD32] Utiliza la metafora en la que los programas\nse utilizan como funciones
						*[#9ACD32] Evaluación perezosa: Se evaluan ecuaciones\nque son estrictamente necesarias para hacer\ncálculos posteriores
					*_ Características
						*[#9ACD32] Existencia de funciones de orden superior
						*[#9ACD32] No hay una distinción entre datos y programas
						*[#9ACD32] Hace enfasis en los aspectos creativos de la programación
				*[#00BFFF] Lógica
					*_ Donde
						*[#9ACD32] Se acude a la lógica de predicados de primer orden
							*_ y 
								*[#8FBC8F] se obtienen predicados que obtienen relaciones\n entre objetos que se están definiendo
					*_ Basado en 
						*[#9ACD32] Modelo de la demostración de la lógica
						*[#9ACD32] Modelo de la demostración automática
					*_ Características
						*[#9ACD32] Se permite ser más declarativa
						*[#9ACD32] Se utilizan como expresiones de los programas
						*[#9ACD32] Axiomas y reglas de inferencia
						*[#9ACD32] Se puede definir una relación factorial
						*[#9ACD32] El compilador es un motor de interferencias
							*_ el cual a partir de expresiones
								*[#8FBC8F] razona y da respuesta a nuestras preguntas.
			*_ Ventajas
				*[#00BFFF] Ventaja evidente respecto a la programación Imperativa:
					*[#9ACD32] Programas más cortos
					*[#9ACD32] Programas mucho más fáciles de realizar
					*[#9ACD32] Programas más fáciles de depurar
					*[#9ACD32] Los tiempos de modificación y depuración son mucho menor
				*[#00BFFF] El nivel adecuado de interacción escojido, puede estar\ndemasiado alejado de la orden inicial.
			*_ Desventajas
				*[#00BFFF] Tiempo de desarrollo de programación\nmucho menor.

@endmindmap
```
### Vídeos de referencia
1. [Programación Declarativa. Orientaciones y pautas para el estudio (1998, 1999, 2001)](https://canal.uned.es/video/5a6f2c66b1111f54718b4911)
2. [Programación Declarativa. Orientaciones y pautas para el estudio (1998, 1999, 2001)](https://canal.uned.es/video/5a6f2c5bb1111f54718b488b)
3. [Programación Declarativa. Orientaciones y pautas para el estudio (1998, 1999, 2001)](https://canal.uned.es/video/5a6f2c42b1111f54718b4757)

# Mapa conceptual 2
## Lenguajes de Programación Funcional (2015)

```plantuml
@startmindmap
*[#FF4500] Lenguaje de Programación Funcional 
	*_ Hay dos tipos
		*[#F08080] Paradigma Hiperativo
			*_ Su ejecución es
				*[#00BFFF] Una serie de instrumentos ejecutandose esencialmente
					*_ Es modificable según su
						*[#9ACD32] Estado de cómputo
							*_ Que es
								*[#8FBC8F] Zona de memoria accesible mediante serie de variables
		*[#F08080] Paradigmas de Programación
			*_ son
				*[#00BFFF] Modelos de computación
					*[#9ACD32] Basados en el modelo de Von Neumann
					*_ En las que 
						*[#9ACD32] Diferentes lenguajes datan de semántica en los programas
	*_ Tipos de lenguajes
		*[#F08080] Lógica Simbólica
			*_ es un
				*[#00BFFF] Paradigma de Programación Lógica
					*_ En donde
						*[#9ACD32] Los programas se definen mediante inferencia lógica
		*[#F08080] Funcional
			*[#00BFFF] Lambda Cálculo
				*_ es un
					*[#9ACD32] Sistema para la aplicación de funciones de recursividad
						*_ Da paso a
							*[#8FBC8F] Lógica combinatoria
								*_ Permite
									*[#87CEEB] Modificar el lenguaje de programación
			*_ Son funciones que 
				*[#00BFFF] Reciben parámetros de entrada y salida
					*_ Consecuencia
						*[#9ACD32] Se pierde el concepoto de variable
							*_ da paso a 
								*[#8FBC8F] Desaparición de los bucles
									*_ Mediante
										*[#87CEEB] recursión
											*[#008080] Su definición hace referencia así mismas.
			*_ Se evalua en 
				*[#00BFFF] Evaluación prezosa
					*_ También llamada
						*[#9ACD32] Evaluación no estricta
							*_ Consiste en
								*[#8FBC8F] Evaluar desde afuera hacia adentro
					*_ Diferente a la
						*[#9ACD32] Evaluación impaciente o estricta
							*_ esta
								*[#8FBC8F] Evalua de adetro hacia afuera
					*_ Caracterizada por realizar
						*[#9ACD32] Memoización
							*[#8FBC8F] Almacena el valor de una expresión\n cuyo valor ya ha sido analizado
	*_ Comparado con el lenguaje imperativo
		*[#F08080] Un programa es una colección de datos, variables modificables
			*_ a diferencia del funcional
				*[#00BFFF] Son puramente funciones, instrucciones\nque operan sobre dichos datos
					*_ las 
						*[#9ACD32] Instrucciones se ejecutan en un orden adecuado
							*_ dicho orden puede\n cambiar según
								*[#8FBC8F] el valor de las variables
								*[#8FBC8F] El estado del cómputo que lleva todas las variables.
		*[#F08080] Ya mencionado existen las variables
			*_ a diferencia del funcional
				*[#00BFFF] Existen pero hacen referencia a los parametros\nde entrada de las funciones
		*[#F08080] Una función en este lenguaje puede tener efectos colaterales\ny alterar el estado del cómputo paralelamente a devolver el resultado
			*_ Encontrarse
				*[#00BFFF] Errores no previstos
		*_ las
			*[#F08080] Constantes
				*[#00BFFF] equiparan a las funciones
					*_ si la función 
						*[#9ACD32] devuelve el mismo resultado se puede\npensar que es una función constante
							*_ a su vez
								*[#8FBC8F] que si devuelve el mismo resultado tiene\nque hacer independiente de los parametros de entrada que reciben
								*[#8FBC8F] Si una función no recibe parámetros, siempre devolverá el mismo resultado.
@endmindmap
```
### Vídeos de referencia
1. [Lenguaje de Programación Funcional (2015)](https://canal.uned.es/video/5a6f4bf3b1111f082a8b4708)


## Autor: Vásquez Alcántara Roberto Alejandro 

[VasquezAlcantaraRobertoAlejandro @ GitLab](https://gitlab.com/VasquezAlcantaraRobertoAlejandro)